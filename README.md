# Metall game engine

Metall game engine (The name is inspired by the most common **source** matterial in a **spring** because the engine is inspired by the **spring** engine and the **source** engine, the spelling of "metal" is in swedish because the founders of this repo are swedish).
Metall game engine aims to be a game engine designed for 2d games.

# Roadmap

## Make the base files
- [ ] Make a function that can render something simple
- [X] Make a simple audio engine
- [ ] Make a simple script that can take inputs
## Make them more complex
- [ ] Make a function that parse complex data for rendering

# Setup
## compile from source
### linux (ubuntu)
1. clone the repository
2. install freeglut3-dev by running the following command:
`sudo apt-get install freeglut3-dev`
3. build the source code
This segment has not yet been figured out but is belived to be done with something similar to
`gcc filename.c -lGL -lGLU -lglut ` where filename.c is the name of some file
